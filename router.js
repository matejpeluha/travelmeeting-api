import setRootRoute from "./routes/root.route.js";
import setRegisterRoute from "./routes/registration/registration.route.js";
import setLoginRoute from "./routes/login/login.route.js";

function getRouter(app, express){
    let router = express.Router();

    setRootRoute(router);
    setRegisterRoute(router);
    setLoginRoute(router);

    return router;
}

export default getRouter;