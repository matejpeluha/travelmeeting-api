import bcrypt from 'bcrypt';

import getConnection from "../config/mysql.config.js";

class UserLoginService{
    #connection;
    #mail;
    #password;
    #res;

    constructor(req, res) {
        this.#connection = getConnection();
        this.#mail = req.body.mail;
        this.#password = req.body.password;
        this.#res = res;
    }

    loginUser(){
        const queryString = "SELECT * FROM profiles WHERE mail=?";
        const values = [this.#mail];

        this.#connection.query(queryString, values, this.#handleUserLogin);
    }

    #handleUserLogin = (err, results) =>{
        if(err){
            this.#res.statusCode(500);
        }
        else if(results.length < 1){
            this.#res.status(409).send("Profile does not exist");
        }
        else{
            const profilePassword = results[0].password;
            bcrypt.compare(this.#password, profilePassword, this.#handleLogin);
        }
    }

    #handleLogin = (err, result) =>{
        if(err){
            this.#res.sendStatus(500);
        }
        else if(!result){
            this.#res.status(403).send("Wrong password");
        }
        else{
            this.#res.status(200).send("PRIHLASENY");
        }
        this.#res.end();
    }

}

export default UserLoginService;