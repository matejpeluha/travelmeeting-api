import bcrypt from 'bcrypt';

import getConnection from "../config/mysql.config.js";


class UserRegistrationService{
    #connection;
    #password;
    #mail;
    #firstName;
    #lastName;
    #userId;
    #res;

    constructor(req, res) {
        this.#connection = getConnection();
        this.#password = req.body.password;
        this.#mail = req.body.mail;
        this.#firstName = req.body.first_name;
        this.#lastName = req.body.last_name;
        this.#res = res;
    }

    registerUser(){
        const queryString = "SELECT * FROM profiles WHERE mail=?";
        const values = [this.#mail];

        this.#connection.query(queryString, values, this.#handleUserRegistration);
    }

    #handleUserRegistration = (err, results) =>{
        if(err){
            this.#res.sendStatus(500);
        }
        else if(results.length > 0){
            this.#res.status(409).send("Mail already used");
        }
        else{
            this.#startUserRegistration();
        }
    }

    #startUserRegistration(){
        const queryString = "INSERT INTO users (first_name, last_name) VALUES (?, ?)";
        const values = [this.#firstName, this.#lastName];

        this.#connection.query(queryString, values, this.#finishUserRegistration);
    }

    #finishUserRegistration = (err, result) =>{
        if(err){
            this.#res.sendStatus(500);
            this.#res.end();
        }
        else{
            this.#handleProfileRegistration(result);
        }
    }

    #handleProfileRegistration(result){
        this.#userId = result.insertId;
        const saltRounds = 10;
        bcrypt.hash(this.#password, saltRounds, this.#registerProfile);
    }

    #registerProfile = (err, hashedPassword) =>{
        const queryString = "INSERT INTO profiles (user_id, mail, password) VALUES (?, ?, ?)";
        const values = [this.#userId, this.#mail, hashedPassword];

        this.#connection.query(queryString, values, this.#finishProfileRegistration);
    }

    #finishProfileRegistration = (err, result) =>{
        if(err){
            this.#res.sendStatus(500);
        }
        else{
            this.#res.sendStatus(200);
        }

        this.#res.end();
    }
}

export default UserRegistrationService;