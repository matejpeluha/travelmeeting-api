import express from 'express';
import cors from 'cors';
import bodyParser from 'body-parser';

import getRouter from "./router.js";

//deklaracia expressu a portu
const app = express();
const port = 4000;

//CORS POLICIES
app.use(cors());

//KONFIGURACIA BODY PARSER MIDDLEWARU NA PARSOVANIE TELA REQUESTU
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

/*
REQUESTY
 */
const router = getRouter(app, express);
app.use(router);

/*
SPUSTENIE APPKY
 */
app.listen(port, () => {
    console.log("Server running on port 4000");
});