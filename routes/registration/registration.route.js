import UserRegistrationService from "../../services/user-registration.service.js";

function setRegisterRoute(urlRouter){
    const path = "/registration";

    urlRouter.route(path).post(postRequest);

    return urlRouter;
}

const postRequest = (req, res) =>{
    const userRegistration = new UserRegistrationService(req, res);
    userRegistration.registerUser();
}


export default setRegisterRoute;