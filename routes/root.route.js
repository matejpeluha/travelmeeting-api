function setRootRoute(router){
    const path = "/";

    router.route(path).get(getRequest);
    router.route(path).post(postRequest);
}


const getRequest = (req, res) =>{
    const jsonObject = {answer: "Hello World !?!!"};
    res.json(jsonObject);
}

const postRequest = (req, res) =>{
    const jsonObject = {reqBody: req.body, path: "/"};
    res.json(jsonObject);
}

export default setRootRoute;