import UserLoginService from "../../services/user-login.service.js";

function setLoginRoute(urlRouter){
    const path = "/login";

    urlRouter.route(path).post(postRequest);

    return urlRouter;
}


const postRequest = (req, res) =>{
    const userLoginService = new UserLoginService(req, res);
    userLoginService.loginUser();
}

export default setLoginRoute;