import mysql from "mysql";

function getConnection(){
    const connectionData = {
        host: 'localhost',
        user: 'root',
        database: 'social_network'
    };

    return mysql.createConnection(connectionData);
}

export default getConnection;